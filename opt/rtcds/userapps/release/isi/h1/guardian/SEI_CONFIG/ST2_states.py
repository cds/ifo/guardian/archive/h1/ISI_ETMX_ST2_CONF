# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
# SVN $Id$
# $HeadURL$

from guardian import GuardState, GuardStateDecorator, NodeManager
from ezca.ligofilter import LIGOFilter
from isiguardianlib.ligoblend import LIGOBlendManager
from isiguardianlib.blend import states as blend_states
from isiguardianlib.blend import const as blend_const
from isiguardianlib.sensor_correction import states as sc_states
from isiguardianlib import const as top_const
import const
import re
######################################

SC_ramp_time = 1

##################################################
# Configuration Control
class desired_conf():
    """This will make it a bit more clear what configuration and filter 
    we are calling when writing new states. The purpose was to allow for 
    easy state creation. Hopefully it makes it easier and not more 
    confusing..

    Sample use:
    >>> A = desired_conf('SC', 'A')
    >>> A.Filter_Banks
    ['WNR','FIR']
    >>> A.MATCH_X
    [2,3]
    """
    def __init__(self, BLENDorSC, conf_letter):
        if BLENDorSC == 'BLEND':
            self.blend = True
            self.SC = False
            self.filter_dict = const.BLEND_FILTER_SELECTION
        elif BLENDorSC == 'SC':
            self.SC = True
            self.blend = False
            self.filter_dict = const.SC_FM_confs
        self.conf = conf_letter
        # Run to set attributes
        self.bank_dof()
    @property
    def Filter_Banks(self):
        #return [bank for bank in self.filter_dict[self.conf]]
        banks = [bank for bank in self.filter_dict[self.conf]]
        for bank in banks:
            total_dofs = 0
            for dof in self.filter_dict[self.conf][bank]:
                if self.filter_dict[self.conf][bank][dof]:
                    total_dofs +=1
            if total_dofs == 0:
                banks.remove(bank)
        return banks
    @property
    def Blend_dofs(self):
        if self.blend:
            return [dof for dof in self.filter_dict[self.conf]['BLEND'] \
                        if self.filter_dict[self.conf]['BLEND'][dof]]
        else:
            return None
    @property
    def SC_dofs(self):
        if self.SC:
            SCdict = {}
            for bank in self.Filter_Banks:
                SCdict[bank] = [dof for dof in self.filter_dict[self.conf][bank] \
                        if self.filter_dict[self.conf][bank][dof]]
            return SCdict
        else:
            return None
    def bank_dof(self):
        for bank in self.Filter_Banks:
            for dof in self.filter_dict[self.conf][bank]:
                setattr(self, bank+'_'+dof, self.filter_dict[self.conf][bank][dof])

# Config assignments
blend_250 = desired_conf('BLEND', 'A')
blend_750 = desired_conf('BLEND', 'B')

A_SC = desired_conf('SC', 'B')
B_SC = desired_conf('SC', 'A')

##################################################
# States

class INIT(GuardState):
    """Check the status of the blends and SC to determine which state to go to"""
    def main(self):
        self.lbm = LIGOBlendManager(deg_of_free_list=const.CHAMBER_BLEND_FILTERS_DOF, ezca=ezca)
        # A list of tuples containing the number and name respectively of the current filter
        blend_names = {dof : self.lbm.get_cur_blend_name(dof) for dof in const.CHAMBER_BLEND_FILTERS_DOF}

        ## Check if in Blend A
        # Make sure the dof sets are the same
        if set(blend_250.Blend_dofs) == set([dof for dof in blend_names]):
            # Make a new list containing the dof that has a matching engaged filter,
            # and then if the list is still just as long then move on.
            if len([dof for dof in blend_names if blend_names[dof][0] == \
                        getattr(blend_250, 'BLEND_{}'.format(dof))[0]]) \
                        == len(blend_250.Blend_dofs):
                # We are in Blend A !!!

                for dof in const.SC_ENGAGED_DOF:
                    
                    FIR_gain = ezca['SENSCOR_{}_FIR_GAIN'.format(dof)]
                    IIRHP_gain = ezca['SENSCOR_{}_IIRHP_GAIN'.format(dof)]
                    MATCH_gain = ezca['SENSCOR_{}_MATCH_GAIN'.format(dof)]
                    ### FIXME: This only only checks that the first dof matches, and then moves on!
                    if FIR_gain == 0.0 and IIRHP_gain == 0.0 and MATCH_gain == 0.0:
                        return 'BLEND_250_SC_NONE'
                    elif FIR_gain == 1.0 and IIRHP_gain == 0.0 and MATCH_gain == 1.0:
                        return 'BLEND_250_SC_A'
                    elif FIR_gain == 0.0 and IIRHP_gain == 1.0 and MATCH_gain == 1.0:
                        return 'BLEND_250_SC_B'
                    else:
                        # I guess go here???
                        log('SC does not match any known configuration, going to BLEND_250_SC_NONE')
                        return 'BLEND_250_SC_NONE'
            else:
                log('CPA')

        ## Check if in Blend B
        # Make sure the dof sets are the same
        elif set(blend_750.Blend_dofs) == set([dof for dof in blend_names]):
            # Make a new list containing the dof that has a matching engaged filter,
            # and then if the list is still just as long then move on.
            if len([dof for dof in blend_names if blend_names[dof][0] == \
                        getattr(blend_750, 'BLEND_{}'.format(dof))[0]]) \
                        == len(blend_750.Blend_dofs):
                # We are in Blend A !!!

                for dof in const.SC_ENGAGED_DOF:
                    
                    FIR_gain = ezca['SENSCOR_{}_FIR_GAIN'.format(dof)]
                    IIRHP_gain = ezca['SENSCOR_{}_IIRHP_GAIN'.format(dof)]
                    MATCH_gain = ezca['SENSCOR_{}_MATCH_GAIN'.format(dof)]
                    ### FIXME: This only only checks that the first dof matches, and then moves on!
                    if FIR_gain == 0.0 and IIRHP_gain == 0.0 and MATCH_gain == 0.0:
                        return 'BLEND_750_SC_NONE'
                    elif FIR_gain == 1.0 and IIRHP_gain == 0.0 and MATCH_gain == 1.0:
                        return 'BLEND_750_SC_A'
                    elif FIR_gain == 0.0 and IIRHP_gain == 1.0 and MATCH_gain == 1.0:
                        return 'BLEND_750_SC_B'
                    else:
                        # I guess go here???
                        log('SC does not match any known configuration, going to BLEND_750_SC_NONE')
                        return 'BLEND_750_SC_NONE'
            else:
                log('CPB')
                
        else:
            log('Blends are not in a known configuration, going to DOWN')
            return 'DOWN'


####################
# Blend states
    # Just grab the first FM# since they should be the same anyway
SWITCH_TO_750_BLEND = blend_states.get_basic_blend_switch_state(
    deg_of_free_list=blend_750.Blend_dofs,
    blend_number=getattr(blend_750, 'BLEND_{}'.format(blend_750.Blend_dofs[0]))[0],
    requestable=False)

SWITCH_TO_250_BLEND = blend_states.get_basic_blend_switch_state(
    deg_of_free_list=blend_250.Blend_dofs,
    blend_number=getattr(blend_250, 'BLEND_{}'.format(blend_250.Blend_dofs[0]))[0],
    requestable=False)

####################
# SC states

TURN_ON_BLEND_250_A_SC = sc_states.get_SC_switch_one_bank(
    A_SC.Filter_Banks[0],
    A_SC.SC_dofs[A_SC.Filter_Banks[0]],
    1,
    ramp_time=1)

TURN_ON_BLEND_250_B_SC = sc_states.get_SC_switch_one_bank(
    B_SC.Filter_Banks[0],
    B_SC.SC_dofs[B_SC.Filter_Banks[0]],
    1,
    ramp_time=1)

TURN_ON_BLEND_750_A_SC = sc_states.get_SC_switch_one_bank(
    A_SC.Filter_Banks[0],
    A_SC.SC_dofs[A_SC.Filter_Banks[0]],
    1,
    ramp_time=1)

TURN_ON_BLEND_750_B_SC = sc_states.get_SC_switch_one_bank(
    B_SC.Filter_Banks[0],
    B_SC.SC_dofs[B_SC.Filter_Banks[0]],
    1,
    ramp_time=1)



# Three of the same states but with different names to make a longer path so it doesn't skip
TURN_OFF_BLEND_250_SC = sc_states.get_SC_all_gains_off(
    A_SC.Filter_Banks + B_SC.Filter_Banks,
    A_SC.SC_dofs[A_SC.Filter_Banks[0]] + B_SC.SC_dofs[B_SC.Filter_Banks[0]], 
    ramp_time=1)

TURN_OFF_BLEND_750_SC = sc_states.get_SC_all_gains_off(
    A_SC.Filter_Banks + B_SC.Filter_Banks,
    A_SC.SC_dofs[A_SC.Filter_Banks[0]] + B_SC.SC_dofs[B_SC.Filter_Banks[0]], 
    ramp_time=1)

####################
# Idle states

def get_idle_state(index):
    class IDLE_STATE(GuardState):
        def main(self):
            return True
        def run(self):
            return True
    IDLE_STATE.index = index
    return IDLE_STATE
        

##########
# 250s
BLEND_250_SC_A = get_idle_state(28)
BLEND_250_SC_B = get_idle_state(27)
BLEND_250_SC_NONE = get_idle_state(25)
##########
# 750s
BLEND_750_SC_A = get_idle_state(78)
BLEND_750_SC_B = get_idle_state(77)
BLEND_750_SC_NONE = get_idle_state(70)
##########

class DOWN(GuardState):
    """This state is more of just a parking spot for the node to go
    when someone wants to change the configuration to something other
    than what is defined here, or if there is a problem it can jump to DOWN.
    """
    goto = True
    index = 1
    def main(self):
        return True
    def run(self):
        return True

##################################################
# Edges

edges = [
    # Blend Changes
    ('BLEND_250_SC_NONE', 'SWITCH_TO_750_BLEND'),
    ('SWITCH_TO_750_BLEND', 'BLEND_750_SC_NONE'),
    ('BLEND_750_SC_NONE', 'SWITCH_TO_250_BLEND'),
    ('SWITCH_TO_250_BLEND', 'BLEND_250_SC_NONE'),
    # 250 SC
    ('BLEND_250_SC_NONE', 'TURN_ON_BLEND_250_A_SC'),
    ('TURN_ON_BLEND_250_A_SC', 'BLEND_250_SC_A'),
    ('BLEND_250_SC_A', 'TURN_OFF_BLEND_250_SC'),

    ('TURN_OFF_BLEND_250_SC', 'BLEND_250_SC_NONE'),

    ('BLEND_250_SC_NONE', 'TURN_ON_BLEND_250_B_SC'),
    ('TURN_ON_BLEND_250_B_SC', 'BLEND_250_SC_B'),
    ('BLEND_250_SC_B', 'TURN_OFF_BLEND_250_SC'),
    # 750 SC
    ('BLEND_750_SC_NONE', 'TURN_ON_BLEND_750_A_SC'),
    ('TURN_ON_BLEND_750_A_SC', 'BLEND_750_SC_A'),
    ('BLEND_750_SC_A', 'TURN_OFF_BLEND_750_SC'),

    ('TURN_OFF_BLEND_750_SC', 'BLEND_750_SC_NONE'),

    ('BLEND_750_SC_NONE', 'TURN_ON_BLEND_750_B_SC'),
    ('TURN_ON_BLEND_750_B_SC', 'BLEND_750_SC_B'),
    ('BLEND_750_SC_B', 'TURN_OFF_BLEND_750_SC'),
    ]
